<?php

/**
 * Add body classes if certain regions have content.
 */
function simpleflex_preprocess_html(&$variables) {
}

/**
 * Override or insert variables into the page template for HTML output.
 */
function simpleflex_process_html(&$variables) {
}

/**
 * Override or insert variables into the page template.
 */
function simpleflex_process_page(&$variables) {
  if (isset($vars['node'])) {
    $vars['theme_hook_suggestion'] = 'page__'.$vars['node']->type;
  }
}


/**
 * Implements hook_preprocess_maintenance_page().
 */
function simpleflex_preprocess_maintenance_page(&$variables) {
  // By default, site_name is set to Drupal if no db connection is available
  // or during site installation. Setting site_name to an empty string makes
  // the site and update pages look cleaner.
  // @see template_preprocess_maintenance_page
  if (!$variables['db_is_active']) {
    $variables['site_name'] = '';
  }
  drupal_add_css(drupal_get_path('theme', 'nccic2') . '/css/maintenance-page.css');
}

/**
 * Override or insert variables into the maintenance page template.
 */
function simpleflex_process_maintenance_page(&$variables) {
}

/**
 * Override or insert variables into the node template.
 */
function simpleflex_preprocess_node(&$variables) {
}

/**
 * Override or insert variables into the block template.
 */
function simpleflex_preprocess_block(&$variables) {
  // In the header region visually hide block titles.
  if ($variables['block']->region == 'header') {
    $variables['title_attributes_array']['class'][] = 'element-invisible';
  }
}

/**
 * Implements theme_menu_tree().
 */
function simpleflex_menu_tree($variables) {
  return '<ul class="menu clearfix">' . $variables['tree'] . '</ul>';
}

/**
 * Implements theme_field__field_type().
 */
function simpleflex_field__taxonomy_term_reference($variables) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h3 class="field-label">' . $variables['label'] . ': </h3>';
  }

  // Render the items.
  $output .= ($variables['element']['#label_display'] == 'inline') ? '<ul class="links inline">' : '<ul class="links">';
  foreach ($variables['items'] as $delta => $item) {
    $output .= '<li class="taxonomy-term-reference-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
  }
  $output .= '</ul>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '"' . $variables['attributes'] .'>' . $output . '</div>';

  return $output;
}
